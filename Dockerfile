FROM node:alpine

WORKDIR /app
COPY . .
RUN yarn
# Build the typescript app
RUN yarn run build

VOLUME [ "/app/data" ]
EXPOSE 8000
CMD yarn run start