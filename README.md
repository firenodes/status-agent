# Status Agent

## Prerequisites

- Docker & [Docker Compose CLI Plugin](https://gist.github.com/creepinson/2363097a783246de0635c737c1545de7)
- Alternatively, Node.JS and Yarn

## Cloning the Repository

```bash
git clone https://gitlab.com/firenodes/status-agent
cd status-agent
```

## Creating a Environment File

An environment file (**.env**) is where you store the settings such as credentials to a database. Below is an example of the status agent's .env file that you will need to copy and edit.

```env
PORT=8000
```

## Running The Agent With Docker

```bash
docker-compose up -d
```

## Running The Agent Normally

```bash
yarn run build
yarn run start
```

## Documentation

An improved documentation website will be coming out soon.
