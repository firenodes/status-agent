import knex, { Knex } from "knex";
import si from "systeminformation";

export interface Info {
    /**
     * CPU Usage Percent
     */
    cpuUsage: number;
    ramUsage: number;
    status: string;
    timestamp: string;
}

export class Collector {
    db!: Knex;

    constructor(path: string) {
        this.db = knex({
            client: "sqlite3",
            connection: {
                filename: path,
            },
            debug: process.env.DEBUG === "true",
            useNullAsDefault: true,
        });
    }

    async init(migrate: boolean) {
        try {
            const tb = (table: Knex.CreateTableBuilder) => {
                table.increments(), table.string("status");
                table.string("timestamp");
                table.double("cpuUsage");
                table.double("ramUsage");
            };
            if (migrate && (await this.db.schema.hasTable("info"))) {
                await this.db.schema.dropTableIfExists("info");
                await this.db.schema.createTable("info", tb);
            } else await this.db.schema.createTable("info", tb);
        } catch (err) {
            console.log(`Failed to migrate table: ${err.message}`);
        }
    }

    async collectNow() {
        const ram = await si.mem();
        const cpu = await si.currentLoad();

        const data: Info = {
            cpuUsage: cpu.currentLoad,
            ramUsage: (ram.active / ram.total) * 100,
            status: "ok",
            timestamp: new Date().toISOString(),
        };
        await this.db<Info>("info").insert(data);
        return data;
    }

    async selectAll() {
        return await this.db<Info>("info").select();
    }

    async selectByDate(fromDate: Date, toDate: Date) {
        return await this.db<Info>("info")
            .where("timestamp", ">=", fromDate.toISOString())
            .where("timestamp", "<=", toDate.toISOString())
            .select();
    }
}
