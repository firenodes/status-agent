import express from "express";
import { Collector } from "./collector";
import { mkdirSync } from "fs";
import { config } from "dotenv";

const main = async () => {
    config();

    try {
        mkdirSync("data");
    } catch {}

    const port = process.env.PORT || 8000;
    const app = express();
    const collector = new Collector("data/db.sqlite3");

    await collector.init(process.env.MIGRATE === "true");

    setInterval(() => {
        collector.collectNow();
    }, parseInt(process.env.INTERVAL || "15000"));

    app.get("/data/latest", async (req, res) => {
        const data = await collector.selectAll();
        res.json(data);
    });

    app.get("/data/between", async (req, res) => {
        const fromDate = new Date(req.query.from as string);
        const toDate = new Date(req.query.to as string);
        const data = await collector.selectByDate(fromDate, toDate);
        res.json(data);
    });

    app.listen(port, () => console.log(`Agent listening on :${port}`));
};

main();
